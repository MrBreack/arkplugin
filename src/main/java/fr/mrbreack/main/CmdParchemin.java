package fr.mrbreack.main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CmdParchemin implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        Player p = ( Player ) sender;
        if (sender instanceof Player) {


            ItemStack Bed = new ItemStack(Material.GLOBE_BANNER_PATTERN);
            ItemMeta BedM = Bed.getItemMeta();
            BedM.setDisplayName(("&6&l• Parchemin de téléportation").replace("&", "§"));

            List <String> lores = new ArrayList <>();

            lores.add(ChatColor.WHITE + "Ce parchemin de téléportation vous permet");
            lores.add(ChatColor.WHITE +"de rejoindre un lieu qui vous est familier.");
            lores.add(ChatColor.GRAY +"§oVous sentez la puissance magique de l'objet");
            lores.add(ChatColor.GRAY +"§ovous insuffler des visions de lieux familiers.");

            BedM.setLore(lores);
            Bed.setItemMeta(BedM);

            p.getInventory().addItem(Bed);
        }
        return false;
    }

}
